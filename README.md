# Cargo GitReg

A minimal private crate registry using existing infrastructure.

`cargo-gitreg` leverages existing git repositores to provide secure access to
private crates. As of this writing (2020-03-08), while registry API and index
access is authenticated, access to crate source is *not*. Even if a private
registry provided a mechanism to authenticate crate downloads, `cargo` itself
will not send authentication information for them. This poses a problem for
organizations that require authentication for source access internally,
whether for more granular access control, or simply as an added security
measure.

GitReg solves this problem by moving the registry to the developer's local
machine and using existing git authentication mechanisms to fetch crate
sources. This way, GitReg performs the necessary authentication and presents
`cargo` with the unauthenticated download endpoint that it expects.

## How It Works

GitReg uses the standard crate index format. `cargo` clones the index
repository and reads the `config.json` to determine where to fetch crate
sources from and where to find the API endpoint.

Because GitReg runs locally, the `config.json` will look something like this:

```json
{
  "dl": "http://localhost:<some port>/crates/{crate}/{version}",
  "api": "http://localhost:<some port>",
}
```

Each GitReg registry must have a unique port allocated to it so that multiple
registries can be run side-by-side. Because this configuration lives in the
index and is read by `cargo`, it cannot be randomized.

The crate index files follow the standard registry format, but also include
the repository URL, which is a requirement for publishing to a GitReg. When a
crate download is requested, GitReg will fetch the crate's repository using
git and check out the tag corresponding to the requested version. To support
multi-crate repositories, it will also attempt to check out a
`<name>-<version>` tag as well. It will then package the crate and serve the
locally-built crate archive.

## Publishing

To publish a crate, a tag corresponding to the version being published must
exist on the remote repository. For example, when publishing crate `foo`
version `0.1.0`, either a tag `foo-0.1.0` or `0.1.0` must exist. They will be
searched for in that order. To ensure that both the tag exists and that it
points to the correct version of the crate, it will be checked out and
packaged during the publish process, and compared to the uploaded archive.

Once the crate source has been verified to match the version in its
repository, the index will be updated, committed, and pushed. By default, a
branch will be created for each publish so that a pull/merge request can be
created for it. The `-d` flag disables this behavior and instead pushes
directly to `master`. Eventually, pushing to a forked repository will be
supported as well.

## Running GitReg

```
$ cargo gitreg -h
cargo-gitreg-gitreg 0.1.0-alpha.0
Josh Robson Chase


USAGE:
    cargo-gitreg gitreg [FLAGS] [OPTIONS] [--] [cmd]...

FLAGS:
    -d               Publish to the index master rather than via branches.
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -b <basedir>        Base directory to clone repositories into. [env: CARGO_GITREG_BASEDIR=]
    -g <git>...         Registry to clone from a git repo. [env: CARGO_GITREG_REPO=]
    -r <reg>...         Registry configured in .cargo/config. [env: CARGO_GITREG_REG=]

ARGS:
    <cmd>...    further commands to run
```

### As a Service

With no additional `cmd` arguments, GitReg will run as a service listening on
`localhost`. All repositories will be cloned into the directory specified by
`-b`, or into `$HOME/.cargo/gitreg` if not provided. Multiple `-r` and `-g`
arguments may be provided to specify the registies to serve. `-g` registries
must be provided as git repository URLs, while `-r` registries will search
cargo configuration for named registry URLs.

### As a Cargo Wrapper

If `cmd` arguments are provided, the registry listeners will be started, and
the extra arguments will be passed to a new `cargo` invocation. Once it
exits, the listeners will be stopped as well. This makes it convenient to use
in CI environments by prefixing normal cargo commands.

## Rough Edges

All git operations are done via the git cli and aren't really checked outside
of the success/failure of the command, so error messages may be incomplete or
misleading. The actual output of the git commands *should* end up either in
the GitReg logs or the API response to cargo though.

There may also be some errors that are reported via http error codes rather
than the expected `200`/`json` response.

### Not Yet Implemented

- [ ] Yank
- [ ] Dealing with forks of registries rather than pushing to master or
      branches