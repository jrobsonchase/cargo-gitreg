use std::collections::{HashMap, HashSet};
use std::env;
use std::fs;
use std::path::{Path, PathBuf};

use anyhow::*;
use serde::Deserialize;

#[derive(Debug, Deserialize, Default)]
pub struct CargoConfig {
    #[serde(default)]
    pub registries: HashMap<String, RegConfig>,
}

#[derive(Debug, Deserialize)]
pub struct RegConfig {
    pub index: String,
}

#[derive(Debug, Deserialize)]
pub struct CargoManifest {
    #[serde(default)]
    pub package: Option<ManifestPackage>,
}

#[derive(Debug, Deserialize)]
pub struct ManifestPackage {
    pub name: String,
    pub version: String,
}

// Shamelessly stolen from the main cargo repository.
fn walk_tree<F>(pwd: &Path, mut walk: F) -> Result<()>
where
    F: FnMut(&Path) -> Result<()>,
{
    let mut stash: HashSet<PathBuf> = HashSet::new();

    for current in pwd.ancestors() {
        let possible = current.join(".cargo").join("config");
        if fs::metadata(&possible).is_ok() {
            walk(&possible)?;
            stash.insert(possible.to_path_buf());
        }
    }

    // Once we're done, also be sure to walk the home directory even if it's not
    // in our history to be sure we pick up that standard location for
    // information.
    let home = env::var("HOME").map_err(|_| {
        anyhow::Error::msg(
            "Cargo couldn't find your home directory. \
             This probably means that $HOME was not set.",
        )
    })?;
    let config = PathBuf::from(home).join(".cargo").join("config");
    if !stash.contains(&config) && fs::metadata(&config).is_ok() {
        walk(&config)?;
    }

    Ok(())
}

pub fn get_config() -> Result<CargoConfig> {
    let mut config = CargoConfig::default();

    walk_tree(&PathBuf::from("."), |path| {
        let tmp: CargoConfig = toml::from_str(
            &fs::read_to_string(path)
                .context(format!("could not read cargo config: {}", path.display()))?,
        )
        .context("could not deserialize cargo config")?;

        for (reg, url) in tmp.registries.into_iter() {
            let _ = config.registries.entry(reg).or_insert(url);
        }
        Ok(())
    })?;

    Ok(config)
}
