use std::collections::HashMap;
use std::io;
use std::path::{Path, PathBuf};
use std::time::Duration;
use std::time::Instant;

use actix::prelude::*;
use url::Url;

use crate::git::Git;

#[derive(Debug)]
pub struct Repo {
    addr: Addr<RepoActor>,
    last_used: Instant,
}

#[derive(Debug)]
pub struct RepoManager {
    basedir: PathBuf,
    repositories: HashMap<Url, Repo>,
}

impl Actor for RepoManager {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        ctx.run_interval(Duration::from_secs(60), |m, _| {
            m.clean_older_than(Duration::from_secs(45))
        });
    }
}

impl RepoManager {
    pub fn new(basedir: impl Into<PathBuf>) -> Self {
        Self {
            basedir: basedir.into(),
            repositories: HashMap::new(),
        }
    }

    pub fn get_repo(&mut self, url: Url) -> Addr<RepoActor> {
        let repo_dir = self
            .basedir
            .join(url.host().expect("git host").to_string())
            .join(&url.path()[1..]);

        self.repositories
            .entry(url.clone())
            .or_insert_with(|| Repo {
                addr: SyncArbiter::start(1, move || RepoActor::new(url.clone(), repo_dir.clone())),
                last_used: Instant::now(),
            })
            .addr
            .clone()
    }

    pub fn clean_older_than(&mut self, since: Duration) {
        let now = Instant::now();

        let urls: Vec<Url> = self
            .repositories
            .iter()
            .filter(|(_, repo)| repo.last_used + since < now)
            .map(|(url, _)| url.clone())
            .collect();

        for url in urls {
            self.repositories.remove(&url);
        }
    }
}

#[derive(Message)]
#[rtype(result = "Result<Addr<RepoActor>, io::Error>")]
pub struct GetRepo(pub Url);

impl Handler<GetRepo> for RepoManager {
    type Result = Result<Addr<RepoActor>, io::Error>;

    fn handle(&mut self, msg: GetRepo, _: &mut Self::Context) -> Self::Result {
        Ok(self.get_repo(msg.0))
    }
}

#[derive(Debug)]
pub struct RepoActor {
    pub git: Git,
    last_fetch: Instant,
}

impl Actor for RepoActor {
    type Context = SyncContext<Self>;
}

impl RepoActor {
    pub fn new(url: Url, basedir: impl Into<PathBuf>) -> Self {
        Self {
            git: Git::new(url, basedir),
            last_fetch: Instant::now() - Duration::from_secs(15),
        }
    }

    pub fn fetch(&mut self) -> io::Result<()> {
        let now = Instant::now();
        if now > self.last_fetch + Duration::from_secs(10) {
            self.last_fetch = now;
            self.git.fetch_latest()?;
        }
        Ok(())
    }

    pub fn checkout(&mut self, rev: impl AsRef<str>) -> io::Result<()> {
        self.fetch()?;
        self.git.orphan()?;
        self.git.reset()?;
        self.git.clean()?;
        self.git.checkout(rev)
    }

    pub fn repo_path(&self) -> &Path {
        self.git.repo_path.as_ref()
    }

    pub fn push(&self, rev: impl AsRef<str>) -> io::Result<()> {
        self.git.push(rev)
    }

    pub fn commit_publish(&self, name: impl AsRef<str>, vers: impl AsRef<str>) -> io::Result<()> {
        self.git.commit_publish(name, vers)
    }
}

#[derive(Message)]
#[rtype(result = "anyhow::Result<R>")]
pub struct RunFn<R: 'static>(pub Box<dyn FnOnce(&mut RepoActor) -> anyhow::Result<R> + Send>);

impl<R> Handler<RunFn<R>> for RepoActor
where
    R: 'static,
{
    type Result = anyhow::Result<R>;

    fn handle(&mut self, msg: RunFn<R>, _: &mut Self::Context) -> Self::Result {
        (msg.0)(self)
    }
}
