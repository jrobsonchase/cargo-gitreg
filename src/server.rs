use std::fs;
use std::path::PathBuf;

use actix::prelude::*;
use actix_web::middleware::Logger;
use actix_web::web;
use actix_web::App;
use actix_web::HttpServer;
use anyhow::{Context, *};
use futures::channel::oneshot;
use log::*;
use url::Url;

use crate::cargo_config::{self, CargoConfig};
use crate::config::ConfigFile;
use crate::download;
use crate::git_actor::*;
use crate::publish;

#[derive(Clone, Debug)]
pub struct SrvCtx {
    pub repomgr: Addr<RepoManager>,
    pub port: u16,
    pub index_url: Url,
    pub direct: bool,
    pub basedir: PathBuf,
}

#[derive(Clone, Debug)]
pub struct Server {
    context: SrvCtx,
}

impl Server {
    pub async fn from_cfg<P: Into<PathBuf>, S: AsRef<str>>(
        repomgr: Addr<RepoManager>,
        basedir: P,
        registry_name: S,
        direct: bool,
    ) -> Result<Server> {
        let mut cargo_config: CargoConfig = cargo_config::get_config()?;

        let url = cargo_config
            .registries
            .remove(registry_name.as_ref())
            .map(|r| r.index)
            .ok_or_else(|| anyhow!("could not find registry {}", registry_name.as_ref()))?;

        Self::from_repo(repomgr, basedir, url, direct).await
    }

    pub async fn from_repo<P: Into<PathBuf>, S: AsRef<str>>(
        repomgr: Addr<RepoManager>,
        basedir: P,
        repo: S,
        direct: bool,
    ) -> Result<Server> {
        let basedir = basedir.into();
        let repo = repo.as_ref();
        info!("Using git repo {}", repo);
        let index_url = Url::parse(repo).context("git repo must be a url")?;

        let index = repomgr.send(GetRepo(index_url.clone())).await??;

        let port = index
            .send(RunFn(Box::new(move |git| {
                git.checkout("origin/master")?;

                let gitdir = git.repo_path().join(".git");
                if !fs::metadata(&gitdir).map(|m| m.is_dir())? {
                    bail!("{} is not a directory", gitdir.display());
                }

                let config_path = git.repo_path().join("config.json");
                if !fs::metadata(&config_path).map(|m| m.is_file())? {
                    bail!("{} is not a file", config_path.display());
                }
                let config_file: ConfigFile =
                    serde_json::from_str(&fs::read_to_string(&config_path)?)
                        .context("Failed to deserialize index config")?;

                let api_uri = Url::parse(&config_file.api).context("failed to parse api url")?;

                let port = api_uri.port_or_known_default().expect("port");
                Ok(port)
            })))
            .await??;

        Ok(Server {
            context: SrvCtx {
                port,
                repomgr,
                basedir,
                index_url,
                direct,
            },
        })
    }

    pub fn start(self) -> Result<impl Future<Output = Result<()>>> {
        let port = self.context.port;

        let server = HttpServer::new(move || {
            App::new()
                .data(self.context.clone())
                .wrap(Logger::default())
                .route("/api/v1/crates/new", web::put().to(publish::handler))
                .route(
                    "/crates/{crate}/{version}",
                    web::get().to(download::handler),
                )
        })
        .bind(&format!("127.0.0.1:{}", port))?
        .run();

        let (tx, rx) = oneshot::channel();

        Arbiter::spawn(async move {
            let res = server.await;
            tx.send(res).unwrap();
        });

        Ok(async move { Ok(rx.await.expect("oneshot canceled")?) })
    }
}
