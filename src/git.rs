use std::fs;
use std::io;
use std::path::PathBuf;
use std::process::{Command, Output};

use log::debug;
use url::Url;

#[derive(Debug)]
pub struct Git {
    pub url: Url,
    pub repo_path: PathBuf,
}

fn make_error(output: Output) -> io::Result<()> {
    if !output.status.success() {
        Err(io::Error::new(
            io::ErrorKind::Other,
            format!(
                "git command failed.\nstdout:\n{}\n\nstderr:\n{}",
                String::from_utf8_lossy(&output.stdout),
                String::from_utf8_lossy(&output.stderr)
            ),
        ))
    } else {
        Ok(())
    }
}
impl Git {
    pub fn new<P: Into<PathBuf>>(url: Url, repo: P) -> Git {
        Git {
            url,
            repo_path: repo.into(),
        }
    }

    fn build_command(&self) -> Command {
        let mut cmd = Command::new("git");
        cmd.current_dir(&self.repo_path);
        cmd
    }

    pub fn fetch_latest(&self) -> io::Result<()> {
        if !fs::metadata(&self.repo_path)
            .map(|m| m.is_dir())
            .unwrap_or(false)
        {
            self.clone()?;
        } else {
            self.fetch()?;
        }
        Ok(())
    }

    fn clone(&self) -> io::Result<()> {
        debug!("cloning {} into {}", self.url, self.repo_path.display());
        let output = Command::new("git")
            .arg("clone")
            .arg(self.url.to_string())
            .arg(&self.repo_path)
            .output()?;

        make_error(output)
    }

    pub fn clean(&self) -> io::Result<()> {
        let output = self.build_command().arg("clean").arg("-ffd").output()?;

        make_error(output)
    }

    pub fn reset(&self) -> io::Result<()> {
        let output = self.build_command().arg("reset").arg("--hard").output()?;

        make_error(output)
    }

    pub fn orphan(&self) -> io::Result<()> {
        let output = self
            .build_command()
            .arg("checkout")
            .arg("--orphan")
            .arg("tmp")
            .output()?;

        make_error(output)
    }

    fn fetch(&self) -> io::Result<()> {
        let output = self
            .build_command()
            .arg("fetch")
            .arg("--tags")
            .arg("--force")
            .output()?;

        make_error(output)
    }

    pub fn checkout(&self, rev: impl AsRef<str>) -> io::Result<()> {
        let output = self
            .build_command()
            .arg("checkout")
            .arg(rev.as_ref())
            .output()?;

        make_error(output)
    }

    pub fn push(&self, remote_ref: impl AsRef<str>) -> io::Result<()> {
        let output = self
            .build_command()
            .arg("push")
            .arg("origin")
            .arg(format!("HEAD:{}", remote_ref.as_ref()))
            .output()?;

        make_error(output)
    }

    pub fn commit_publish(&self, name: impl AsRef<str>, vers: impl AsRef<str>) -> io::Result<()> {
        let output = self.build_command().arg("add").arg(".").output()?;
        make_error(output)?;
        let output = self
            .build_command()
            .arg("commit")
            .arg("-m")
            .arg(format!("Publish {} v{}", name.as_ref(), vers.as_ref()))
            .output()?;
        make_error(output)
    }

    #[allow(dead_code)]
    pub fn commit_yank(&self, name: &str, vers: &str) -> io::Result<()> {
        let output = self.build_command().arg("add").arg(".").output()?;
        make_error(output)?;
        let output = self
            .build_command()
            .arg("commit")
            .arg("-m")
            .arg(format!("Yank {} v{}", name, vers))
            .output()?;
        make_error(output)
    }
}
