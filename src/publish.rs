use std::io;

use actix_web::{web, HttpRequest, HttpResponse, Result};
use bytes::{Buf, BytesMut};
use data_encoding::HEXLOWER;
use futures::StreamExt;
use log::debug;
use ring::digest::{Context, SHA256};
use serde::Serialize;
use url::Url;

use crate::error::ErrorResp;
use crate::git_actor::*;
use crate::index::{self, Index};
use crate::meta::{IndexMeta, PublishMeta};
use crate::server::SrvCtx;

#[derive(Serialize, Debug, Default)]
pub struct PublishResponse {
    pub warnings: PublishWarnings,
}

impl PublishResponse {
    fn with_warning(msg: impl Into<String>) -> Self {
        PublishResponse {
            warnings: PublishWarnings {
                other: vec![msg.into()],
                ..Default::default()
            },
        }
    }
}

#[derive(Serialize, Debug, Default)]
pub struct PublishWarnings {
    pub invalid_categories: Vec<String>,
    pub invalid_badges: Vec<String>,
    pub other: Vec<String>,
}

pub async fn handler(
    ctx: web::Data<SrvCtx>,
    _: HttpRequest,
    mut payload: web::Payload,
) -> Result<web::Json<PublishResponse>> {
    let mut body = BytesMut::new();
    while let Some(item) = payload.next().await {
        body.extend_from_slice(&item?);
    }
    let meta_len = body.get_u32_le();
    let meta = body.split_to(meta_len as usize);
    let crate_len = body.get_u32_le();
    let crate_data = body.split_to(crate_len as usize);
    let mut repacked_crate_data = Vec::new();
    crate::package::repack(io::Cursor::new(crate_data), &mut repacked_crate_data)
        .map_err(|e| api_error!("failed to repack crate: {}", e))?;

    let publish_meta: PublishMeta = serde_json::from_slice(&meta)
        .map_err(|e| api_error!("failed to parse metadata json: {}", e))?;

    let crate_url: Url = publish_meta
        .repository
        .clone()
        .ok_or_else(|| api_error!("missing repository url"))?;

    let verify_vers = publish_meta.vers.clone();
    let verify_name = publish_meta.name.clone();
    let index_meta = IndexMeta::new(publish_meta, &repacked_crate_data);

    // Ensure that the specified tag exists in the remote repo.
    let crate_addr = ctx.repomgr.send(GetRepo(crate_url)).await??;

    let package_sha = crate_addr
        .send(RunFn(Box::new(move |git| {
            git.checkout(&format!("{}-{}", verify_name, verify_vers))
                .map_err(|e| {
                    debug!("failed to find crate-version tag: {}", e);
                    e
                })
                .or_else(|_| {
                    debug!("attempting to check out {}", verify_vers);
                    git.checkout(&verify_vers).map_err(|e| {
                        debug!("failed to find plain version tag: {}", e);
                        e
                    })
                })
                .map_err(|_| {
                    anyhow::anyhow!(
                        "could not find tag {} or {}-{}",
                        verify_vers,
                        verify_name,
                        verify_vers
                    )
                })?;

            let mut package_data = Vec::new();
            crate::package::package_crate(git.repo_path(), &verify_name, &mut package_data)?;
            let mut context = Context::new(&SHA256);
            context.update(&package_data);
            Ok(HEXLOWER.encode(context.finish().as_ref()))
        })))
        .await?
        .map_err(|e| api_error!("{}", e))?;

    if package_sha != index_meta.cksum {
        api_bail!("provided crate data does not match tagged version");
    }

    let index_addr = ctx.repomgr.send(GetRepo(ctx.index_url.clone())).await??;

    let direct = ctx.direct;

    // Pre-create this because index_meta gets moved into the RunFn
    let resp = if !direct {
        PublishResponse::with_warning(format!(
            "pushed branch publish/{}/{}, ask a maintainer to merge it",
            index_meta.name, index_meta.vers
        ))
    } else {
        Default::default()
    };

    let index_path = index::get_path(&index_meta.name);
    index_addr
        .send(RunFn(Box::new(move |git| -> anyhow::Result<()> {
            git.checkout("origin/master")?;

            let index_path = git.repo_path().join(index_path);
            let mut index = Index::read_or_create(&index_path)?;

            if let Some(existing) = index.add(index_meta.clone()) {
                anyhow::bail!("{} v{} already exists", existing.name, existing.vers);
            }

            index.write(index_path)?;
            git.commit_publish(&index_meta.name, &index_meta.vers)?;
            if direct {
                git.push("master")?;
            } else {
                git.push(format!(
                    "refs/heads/publish/{}/{}",
                    index_meta.name, index_meta.vers
                ))?;
            }
            Ok(())
        })))
        .await?
        .map_err(|e| api_error!("{}", e))?;

    Ok(web::Json(resp))
}
