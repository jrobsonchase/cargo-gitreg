use std::collections::HashMap;

use data_encoding::HEXLOWER;
use ring::digest::{Context, SHA256};
use serde::{Deserialize, Serialize};
use url::Url;

#[derive(Copy, Clone, Debug, Deserialize, Serialize)]
pub enum DepKind {
    #[serde(rename = "dev")]
    Dev,
    #[serde(rename = "build")]
    Build,
    #[serde(rename = "normal")]
    Normal,
}

#[derive(Clone, Debug, Deserialize)]
pub struct PublishDep {
    pub name: String,
    pub version_req: String,
    pub features: Vec<String>,
    pub optional: bool,
    pub default_features: bool,
    pub target: Option<String>,
    pub kind: DepKind,
    pub registry: Option<String>,
    pub explicit_name_in_toml: Option<String>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct PublishMeta {
    pub name: String,
    pub vers: String,
    pub deps: Vec<PublishDep>,
    pub features: HashMap<String, Vec<String>>,
    pub authors: Vec<String>,
    pub description: Option<String>,
    pub documentation: Option<String>,
    pub homepage: Option<String>,
    pub readme: Option<String>,
    pub readme_file: Option<String>,
    pub keywords: Vec<String>,
    pub categories: Vec<String>,
    pub license: Option<String>,
    pub license_file: Option<String>,
    pub repository: Option<Url>,
    pub badges: HashMap<String, HashMap<String, String>>,
    pub links: Option<String>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct IndexDep {
    pub name: String,
    pub req: String,
    pub features: Vec<String>,
    pub optional: bool,
    pub default_features: bool,
    pub target: Option<String>,
    pub kind: DepKind,
    pub registry: Option<String>,
    pub package: Option<String>,
}

#[derive(Clone, Deserialize, Serialize, Debug)]
pub struct IndexMeta {
    pub name: String,
    pub vers: String,
    pub deps: Vec<IndexDep>,
    pub cksum: String,
    pub features: HashMap<String, Vec<String>>,
    pub yanked: bool,
    pub links: Option<String>,
    pub repository: Option<Url>,
}

impl From<PublishDep> for IndexDep {
    fn from(other: PublishDep) -> IndexDep {
        IndexDep {
            name: other.name,
            req: other.version_req,
            features: other.features,
            optional: other.optional,
            default_features: other.default_features,
            target: other.target,
            kind: other.kind,
            registry: other.registry,
            package: other.explicit_name_in_toml,
        }
    }
}

impl IndexMeta {
    pub fn new(publish_meta: PublishMeta, data: &[u8]) -> IndexMeta {
        let mut context = Context::new(&SHA256);
        context.update(data);
        let hash = HEXLOWER.encode(context.finish().as_ref());

        IndexMeta {
            name: publish_meta.name,
            vers: publish_meta.vers,
            deps: publish_meta.deps.into_iter().map(IndexDep::from).collect(),
            cksum: hash,
            features: publish_meta.features,
            yanked: false,
            links: publish_meta.links,
            repository: publish_meta.repository,
        }
    }
}
