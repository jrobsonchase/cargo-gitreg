use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct ConfigFile {
    pub api: String,
    pub dl: String,
}
