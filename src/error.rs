use actix_web::HttpResponse;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Error {
    detail: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ErrorResp {
    errors: Vec<Error>,
}

impl ErrorResp {
    pub fn new(detail: String) -> ErrorResp {
        ErrorResp {
            errors: vec![Error { detail }],
        }
    }

    pub fn add(&mut self, detail: String) {
        self.errors.push(Error { detail });
    }
}

impl From<ErrorResp> for HttpResponse {
    fn from(other: ErrorResp) -> HttpResponse {
        HttpResponse::Ok()
            .body(serde_json::to_string(&other).expect("failed to serialize error response"))
    }
}

#[macro_export]
macro_rules! api_error {
    ($($t:tt)*) => {
        HttpResponse::from(ErrorResp::new(format!($($t)*)))
    }
}

#[macro_export]
macro_rules! api_bail {
    ($($t:tt)*) => {
        Err(api_error!($($t)*))?;
    }
}
