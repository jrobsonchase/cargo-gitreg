#[macro_use]
mod error;

mod cargo_config;
mod config;
mod download;
mod git;
mod git_actor;
mod index;
mod meta;
mod package;
mod publish;
mod server;

use std::path::PathBuf;
use std::process::{self, Command};
use std::sync::mpsc;

use actix::prelude::*;
use anyhow::*;
use clap::{App, AppSettings, Arg};
use futures::channel::oneshot;
use futures::future::join_all;
use futures::prelude::*;
use futures::stream::FuturesUnordered;

use crate::git_actor::RepoManager;
use crate::server::Server;

fn main() {
    env_logger::init();
    let sys = System::new("gitreg");
    let res_rx = spawn_main(async_main());
    // Doing this to avoid returning Result<(), Error> from main as it prints
    // the error with Debug, and anyhow brings a huge backtrace.
    // TODO: Make this (and logging) configurable with verbosity flags
    match sys
        .run()
        .map_err(Error::from)
        .and_then(|_| res_rx.recv().map_err(Error::from).and_then(|res| res))
    {
        Err(e) => {
            eprintln!("{}", e);
            process::exit(1);
        }
        Ok(()) => {}
    }
}

fn spawn_main(
    f: impl Future<Output = anyhow::Result<()>> + 'static,
) -> mpsc::Receiver<anyhow::Result<()>> {
    let (res_tx, res_rx) = mpsc::channel();
    Arbiter::spawn(async move {
        let res = f.await;
        let _ = res_tx.send(res);
        System::current().stop();
    });
    res_rx
}

async fn async_main() -> anyhow::Result<()> {
    let matches = App::new("cargo-gitreg")
        .subcommand(
            App::new("gitreg")
                .version(env!("CARGO_PKG_VERSION"))
                .author(env!("CARGO_PKG_AUTHORS"))
                .about(env!("CARGO_PKG_DESCRIPTION"))
                .setting(AppSettings::TrailingVarArg)
                .arg(
                    Arg::with_name("direct")
                        .help("Publish to the index master rather than via branches.")
                        .env("CARGO_GITREG_DIRECT")
                        .required(false)
                        .takes_value(false)
                        .short("d"),
                )
                .arg(
                    Arg::with_name("basedir")
                        .help("Base directory to clone repositories into.")
                        .env("CARGO_GITREG_BASEDIR")
                        .required(false)
                        .takes_value(true)
                        .short("b"),
                )
                .arg(
                    Arg::with_name("git")
                        .help("Registry to clone from a git repo.")
                        .env("CARGO_GITREG_REPO")
                        .required(false)
                        .multiple(true)
                        .number_of_values(1)
                        .short("g"),
                )
                .arg(
                    Arg::with_name("reg")
                        .help("Registry configured in .cargo/config.")
                        .env("CARGO_GITREG_REG")
                        .required(false)
                        .multiple(true)
                        .number_of_values(1)
                        .conflicts_with("git")
                        .short("r"),
                )
                .arg(Arg::from_usage("<cmd>... 'further commands to run'").required(false)),
        )
        .get_matches();

    let gitreg = matches
        .subcommand_matches("gitreg")
        .expect("must be invoked with the 'gitreg' subcommand");

    let basedir = gitreg
        .value_of("basedir")
        .map(PathBuf::from)
        .or_else(|| {
            std::env::var("HOME")
                .map(|h| PathBuf::from(h).join(".cargo"))
                .ok()
        })
        .unwrap_or_else(|| PathBuf::from(std::env::temp_dir()))
        .join("gitreg");

    let direct = gitreg.is_present("direct");
    let repomgr = RepoManager::new(&basedir).start();

    let cfg_servers = join_all(
        gitreg
            .values_of("reg")
            .into_iter()
            .flatten()
            .map(|r| Server::from_cfg(repomgr.clone(), &basedir, r, direct)),
    )
    .await;

    let repo_servers = join_all(
        gitreg
            .values_of("git")
            .into_iter()
            .flatten()
            .map(|g| Server::from_repo(repomgr.clone(), &basedir, g, direct)),
    )
    .await;

    let mut running_servers: FuturesUnordered<_> = cfg_servers
        .into_iter()
        .chain(repo_servers.into_iter())
        .map(|s| s?.start().map(|f| f.boxed()))
        .collect::<Result<_>>()?;

    if let Some(args) = gitreg.values_of("cmd") {
        let mut cargo_cmd = Command::new("cargo");
        for arg in args {
            cargo_cmd.arg(arg);
        }

        let (status_tx, status_rx) = oneshot::channel();

        std::thread::spawn(move || {
            let _ = status_tx.send(cargo_cmd.status());
        });

        if !status_rx.await??.success() {
            bail!("cargo command exited unsuccessfully");
        }
    } else {
        while let Some(res) = running_servers.next().await {
            res?
        }
    }
    Ok(())
}

