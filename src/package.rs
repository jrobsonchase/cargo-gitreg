use std::env;
use std::fs;
use std::io::{Read, Write};
use std::path::{Path, PathBuf};
use std::process::Command;

use flate2::read::GzDecoder;
use flate2::write::GzEncoder;
use flate2::Compression;
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use tar::{Archive, Builder};
use walkdir::{DirEntry, WalkDir};

pub fn temp_dir() -> PathBuf {
    let rand_string: String = thread_rng().sample_iter(&Alphanumeric).take(10).collect();
    let dir = env::temp_dir().join("gitreg").join(rand_string);
    fs::create_dir_all(&dir).expect("failed to create temp dir");
    dir
}

/// Repack a crate deterministicly
pub fn repack(from: impl Read, to: impl Write) -> anyhow::Result<()> {
    let dir = temp_dir();
    let tar = GzDecoder::new(from);
    let mut archive = Archive::new(tar);
    archive.set_preserve_mtime(false);
    archive.unpack(&dir)?;
    let mut compressor = GzEncoder::new(to, Compression::fast());
    {
        let mut builder = Builder::new(&mut compressor);
        builder.mode(tar::HeaderMode::Deterministic);
        for entry in WalkDir::new(&dir)
            .into_iter()
            .filter_map(|e| e.ok())
            .filter(|e| e.file_type().is_file())
        {
            let archive_path = entry.path().strip_prefix(&dir).unwrap();
            builder.append_path_with_name(entry.path(), archive_path)?;
        }
        builder.finish()?;
    }
    compressor.try_finish()?;
    fs::remove_dir_all(dir)?;
    Ok(())
}

pub fn package_crate(
    dir: impl AsRef<Path>,
    name: impl AsRef<str>,
    out: impl Write,
) -> anyhow::Result<()> {
    fn not_target(dir: &DirEntry) -> bool {
        dir.file_name()
            .to_str()
            .map(|d| d != "target")
            .unwrap_or(false)
    }

    fn is_manifest(file: &DirEntry) -> bool {
        file.file_type().is_file()
            && file
                .file_name()
                .to_str()
                .map(|f| f == "Cargo.toml")
                .unwrap_or(false)
    }

    let is_crate = |vers: &str, file: &DirEntry| -> bool {
        file.file_type().is_file()
            && file
                .file_name()
                .to_str()
                .map(|f| f == format!("{}-{}.crate", name.as_ref(), vers))
                .unwrap_or(false)
    };

    for manifest in WalkDir::new(&dir)
        .into_iter()
        .filter_entry(not_target)
        .filter_map(|e| e.ok())
        .filter(is_manifest)
    {
        let contents = fs::read_to_string(manifest.path())?;
        let package = if let Some(package) =
            toml::from_str::<crate::cargo_config::CargoManifest>(&contents)?.package
        {
            package
        } else {
            continue;
        };

        if package.name == name.as_ref() {
            let status = Command::new("cargo")
                .arg("package")
                .arg("--manifest-path")
                .arg(manifest.path())
                .arg("--no-verify")
                .status()?;

            anyhow::ensure!(status.success(), "cargo package failed");

            let crate_file = WalkDir::new(&dir)
                .into_iter()
                .filter_map(|e| e.ok())
                .filter(|f| is_crate(&package.version, f))
                .nth(0)
                .ok_or_else(|| {
                    anyhow::anyhow!(
                        "failed to find crate file {}-{}.crate in {}",
                        package.name,
                        package.version,
                        dir.as_ref().display(),
                    )
                })?;

            return repack(fs::File::open(crate_file.path())?, out);
        }
    }

    anyhow::bail!(
        "failed to find package {} in {}",
        name.as_ref(),
        dir.as_ref().display(),
    );
}
