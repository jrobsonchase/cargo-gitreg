use std::fs;
use std::io::{self, Write};
use std::path::{Path, PathBuf};

use crate::meta::IndexMeta;

pub fn get_path(crate_name: &str) -> PathBuf {
    assert!(crate_name.is_ascii());
    let mut crate_name = String::from(crate_name);
    crate_name.make_ascii_lowercase();
    match crate_name.len() {
        1 => PathBuf::from("1"),
        2 => PathBuf::from("2"),
        3 => PathBuf::from("3").join(&crate_name[0..1]),
        _ => PathBuf::from(&crate_name[0..2]).join(&crate_name[2..4]),
    }
    .join(crate_name)
}

#[derive(Clone, Debug)]
pub struct Index {
    pub versions: Vec<IndexMeta>,
}

impl Index {
    pub fn read<P: AsRef<Path>>(path: P) -> io::Result<Index> {
        Ok(Index {
            versions: fs::read_to_string(path)?
                .lines()
                .map(|line| {
                    serde_json::from_str(&line).map_err(|_| io::ErrorKind::InvalidData.into())
                })
                .collect::<io::Result<Vec<IndexMeta>>>()?,
        })
    }

    pub fn create<P: AsRef<Path>>(path: P) -> io::Result<()> {
        if let Some(parent) = path.as_ref().parent() {
            fs::create_dir_all(parent)?;
        }

        fs::write(path, "")?;

        Ok(())
    }

    pub fn read_or_create<P: AsRef<Path>>(path: P) -> io::Result<Index> {
        if fs::metadata(path.as_ref()).is_err() {
            Index::create(path.as_ref())?
        }

        Index::read(path)
    }

    pub fn write<P: AsRef<Path>>(&self, path: P) -> io::Result<()> {
        let mut file = fs::File::create(&path)?;
        for version in &self.versions {
            writeln!(
                file,
                "{}",
                serde_json::to_string(version).expect("failed to serialize index metadata - BUG?")
            )?;
        }
        Ok(())
    }

    /// Add a new version to the index. If that version already exists, it will
    /// *not* be added and a reference to the old one will be returned.
    pub fn add(&mut self, new_version: IndexMeta) -> Option<&IndexMeta> {
        self.versions.push(new_version);

        let new_vers = &self.versions.last().unwrap().vers;

        let new_idx = self.versions.len() - 1;
        let mut prev_idx = None;

        for (i, version) in self.versions.iter().enumerate() {
            if version.vers == *new_vers && i != new_idx {
                prev_idx = Some(i)
            }
        }

        if let Some(idx) = prev_idx {
            self.versions.pop();
            Some(&self.versions[idx])
        } else {
            None
        }
    }

    #[allow(dead_code)]
    pub fn yank(&mut self, version: &str) {
        self.versions.iter_mut().for_each(|v| {
            if v.vers == version {
                v.yanked = true;
            }
        });
    }
}
