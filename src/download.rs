use actix_web::{web, HttpRequest, HttpResponse, Result};
use log::debug;
use serde::Serialize;
use url::Url;

use crate::git_actor::*;
use crate::index::{self, Index};
use crate::server::SrvCtx;

#[derive(Serialize, Debug, Default)]
pub struct PublishResponse {
    pub warnings: PublishWarnings,
}

#[derive(Serialize, Debug, Default)]
pub struct PublishWarnings {
    pub invalid_categories: Vec<String>,
    pub invalid_badges: Vec<String>,
    pub other: Vec<String>,
}

pub async fn handler(ctx: web::Data<SrvCtx>, req: HttpRequest) -> Result<HttpResponse> {
    let name: String = req
        .match_info()
        .query("crate")
        .parse()
        .expect("Missing crate name");
    let version: String = req
        .match_info()
        .query("version")
        .parse()
        .expect("Missing crate version");

    let index_addr = ctx.repomgr.send(GetRepo(ctx.index_url.clone())).await??;

    let index_path = index::get_path(&name);
    let search_version = version.clone();
    let search_name = name.clone();
    let crate_url = index_addr
        .send(RunFn(Box::new(move |actor| -> anyhow::Result<Url> {
            actor.checkout("origin/master")?;

            let index_path = actor.repo_path().join(index_path);
            let index = Index::read_or_create(&index_path)?;

            debug!(
                "searching index for crate with name {} and version {}",
                search_name, search_version
            );

            for index_vers in index.versions {
                if index_vers.vers == search_version {
                    return Ok(index_vers.repository.ok_or_else(|| {
                        anyhow::anyhow!(
                            "missing repository url for {}-{}",
                            search_name,
                            search_version
                        )
                    })?);
                }
            }

            anyhow::bail!("failed to find crate {}-{}", search_name, search_version);
        })))
        .await?
        .expect("FIXME");

    let crate_addr = ctx.repomgr.send(GetRepo(crate_url)).await??;

    let crate_data = crate_addr
        .send(RunFn(Box::new(move |git| {
            git.checkout(&format!("{}-{}", name, version))
                .or_else(|_| git.checkout(&version))?;

            let mut package_data = Vec::new();
            crate::package::package_crate(git.repo_path(), &name, &mut package_data)?;
            Ok(package_data)
        })))
        .await?
        .expect("FIXME");

    Ok(HttpResponse::Ok().body(crate_data))
}
