# syntax=docker/dockerfile:1.0.0-experimental

FROM rust AS dependencies

RUN mkdir /build

WORKDIR /build

COPY Cargo.toml /build/
COPY Cargo.lock /build/

ENV CARGO_TARGET_DIR=/target 

RUN mkdir src && echo "fn main() {}" > src/main.rs && \
  cargo build --release

FROM dependencies AS build

COPY src /build/src
RUN --mount=type=cache,from=dependencies,source=/target,target=/target \
  touch src/main.rs && \
  cargo build --release && \
  mv /target/release/cargo-gitreg /

FROM rust AS rust

COPY --from=build /cargo-gitreg /usr/local/cargo/bin/cargo-gitreg

FROM rust AS service

RUN useradd -m -d /gitreg gitreg

COPY /docker/entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
