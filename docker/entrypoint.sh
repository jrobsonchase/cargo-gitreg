#!/bin/bash

touch /credentials
touch /cargo-config

mkdir -p /gitreg/.cargo
cp /credentials /gitreg/credentials
cp /cargo-config /gitreg/.cargo/config

chown gitreg:gitreg /gitreg/credentials
chown gitreg:gitreg /gitreg/.cargo/config

echo "Starting gitreg with args $@"

su gitreg -c "git config --global credential.helper 'store --file /gitreg/credentials'"
su gitreg -c "git config --global user.name 'Cargo GitReg'"
su gitreg -c "git config --global user.email 'gitreg@noreply'"
su gitreg -c "cargo gitreg -b /gitreg $*"
